@extends('app')

@section('content')
<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">

				<div class="panel panel-default">
					<div class="panel-heading">Popular questions</div>
					<div class="panel-body">
						@include('question.list', ['questions' => $popular_questions])
					</div>
					<!--<div class="list-group" id="popular_questions">

					</div>-->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">New questions</div>
					<div class="panel-body">
						@include('question.list', ['questions' => $new_questions, 'block_class' => 'new-questions'])
					</div>
					<!--<div class="list-group" id="lazyScrollLoading">
						<ul id="lazyScrollLoading">

						</ul>
					</div>-->
				</div>
			</div>
		</div>
</div>

<script src="/js/new_questions_loader.js"></script>
@endsection
