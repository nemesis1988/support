@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="title"><h3>Question View</h3></div>
                <div class="panel panel-default">
                    <div class="panel-heading">Question title: {{ $question->title }}</div>

                    <div class="panel-body">
                        <h4 class="panel-info">Question: {{ $question->question }}</h4>
                        @if ($question->answer)
                            <h4 class="panel-info">Answer: {{ $question->answer }}</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
