<div class="list-group
@if (isset($block_class))
    {{ $block_class }}
@endif">
    @foreach($questions as $question)
        <a href="/question/show/{{ $question->id }}">
            <div class="list-group-item
                @if (!$question->is_moderated)
                    list-group-item-danger
                @endif
                question" id="{{ $question->id }}">

                    <h4 class="question-title" >{{ $question->title }}</h4>
                    <!--<div class="hidden" id="question_text_{{ $question->id }}">
                        <h5 class="list-group-item-text">Question: {{ $question->question }}</h5>
                        @if ($question->answer)
                            <h5 class="list-group-item-text">Answer: {{ $question->answer }}</h5>
                        @endif
                    </div>-->
            </div>
        </a>
    @endforeach
</div>