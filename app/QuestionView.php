<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionView extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'question_view';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question_id', 'country_id', 'user_id'];

    public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }
}
