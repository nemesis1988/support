<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Question extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'question';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'question', 'is_moderated'];

    /**
     * Получение новых вопросов
     *
     * @return mixed
     */
    public static function getNew()
    {
        return self::where('is_moderated', 1)->orderBy('id','desc')->paginate(10);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function questionViews()
    {
        return $this->hasMany('App\QuestionView');
    }

    /**
     * Топ 5 популярных вопросов с сортировкой по стране текущего пользователя
     * если не залогинен - ТОП по общему количеству просмотров
     *
     * @param $query
     * @return mixed
     */
    public function scopeGetPopular($query)
    {
        $query->select(DB::raw('question.*, count(*) as count'))
            ->leftJoin('question_view', function($join) {
                $join->on('question.id', '=', 'question_view.question_id');
                if (Auth::user()) {
                    $join->on('question_view.country_id', '=', DB::raw(Auth::user()->country_id));
                }
            })
            ->where('question.is_moderated', 1)
            ->whereIn('question.answer', ['null',''])
            //->whereNull('question.deleted_at')// лишнее
            ->groupBy('question.id')
            ->orderBy('count','desc')
            ->limit(5);

        return $query->get();
    }

    /**
     * Переопределяю метод, что бы записать юзера
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array  $options
     * @return bool
     */
    protected function performInsert(Builder $query, array $options = [])
    {
        $this->user_id = Auth::user()->id;
        return parent::performInsert($query,$options);
    }
}
