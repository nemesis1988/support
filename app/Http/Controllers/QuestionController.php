<?php

namespace App\Http\Controllers;

use App\Question;
use App\QuestionView;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class QuestionController extends Controller
{
    /**
     * Получение списка популярных вопросов по стране пользователя
     * Если пользователь не вошел - возвращается пустой список
     * @return \Illuminate\Http\Response
     */
    public function getPopular()
    {
        return Question::getPopular();
    }

    /**
     * Главная страница, со списком популярных и всех вопросов
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $new_questions = $this->getNew();
        $popular_questions = $this->getPopular();

        return view('home', ['new_questions' => $new_questions,'popular_questions' => $popular_questions]);
    }

    /**
     * Получение новых (только отмодерированных) вопросов с пагинацией
     * Права не проверяются
     *
     * @return mixed
     */
    public function getNew()
    {
        return Question::getNew();
    }

    /**
     * Отрисовка страницы создания сообщение (называется не красиво)
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        return view('question.create');
    }

    /**
     * Сохранение вопроса
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        Question::create([
            'title' => $request->input('title'),
            'question' => $request->input('question'),
        ]);

        return redirect('/question/my');
    }

    /**
     * Список вопросов пользователя (вместе с неподтвержденными)
     *
     * @return \Illuminate\View\View
     */
    public function getMy()
    {
        $questions = Question::all()->where('user_id', Auth::user()->id);
        return view('question.my',['questions' => $questions]);
    }

    /**
     * Просмотр вопроса
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getShow($id)
    {
        $question = Question::find($id);
        if ($question) {
            $this->getAddView($id);
        }

        return view('question.view',['question'=>$question]);
    }

    /**
     * Добавляем просмотр
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAddView($id)
    {
        $questionView = QuestionView::create([
            'question_id' => $id,
            'user_id' => Auth::user()->id,
            'country_id' => Auth::user()->country_id
        ]);

        return $questionView;
    }


    /**
     * Валидатор
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:255',
            'question' => 'required|string',
            'captcha' => 'required|captcha'
        ]);
    }
}
