<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'QuestionController@getIndex');

Route::get('/home', 'QuestionController@getIndex');

Route::get('question/addView', 'QuestionController@getAddView');
Route::get('question/new', 'QuestionController@getNew');
Route::get('question/popular', 'QuestionController@getPopular');

Route::group(['middleware' => 'auth'], function () {

    Route::controller('question', 'QuestionController');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);


Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
    return $captcha->src($config);
});