<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        $countries = ['Ukraine', 'Russia', 'Poland', 'Belarus'];

        foreach ($countries as $country) {
            DB::table('country')->insert(
                [
                    'name' => $country,
                ]
            );
        }

        $country = DB::table('country')->where('name','Ukraine')->first();
        DB::table('users')
            ->where('email', 'admin@admin.admin')
            ->update(['country_id' => $country->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')
            ->where('email', 'admin@admin.admin')
            ->update(['country_id' => null]);
        Schema::drop('country');
    }
}
