<?php

return [
    'title' => 'Questions',
    'single' => 'question',
    'model' => \App\Question::class,
    'columns' => [
        'id',
        'user' => [
            'title' => 'User',
            'relationship' => 'user',
            'select' => '(:table).email'
        ],
        'title' => [
            'title' => 'Title'
        ],
        'question' => [
            'title' => 'Question'
        ],
        'created_at' => [
            'title' => 'Question date',
            'type' => 'datetime'
        ],
        'answer' => [
            'title' => 'Answer',
        ],
        'is_moderated' => [
            'title' => 'Is Moderated',
            'type' => 'bool'
        ],
    ],
    'edit_fields' => [
        'title' => [
            'title' => 'Title'
        ],
        'question' => [
            'title' => 'Question'
        ],
        'answer' => [
            'title' => 'Answer',
        ],
        'is_moderated' => [
            'title' => 'Is Moderated',
            'type' => 'bool'
        ],
    ]
];