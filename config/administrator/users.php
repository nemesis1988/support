<?php

return [
    'title' => 'Users',
    'single' => 'user',
    'model' => \App\User::class,
    'columns' => [
        'id',
        'email',
        'country' => [
            'title' => 'Country',
            'relationship' => 'country',
            'select' => '(:table).name'
        ],
        'role' => [
            'title' => 'Role',
            'type' => 'enum',
            'options' => [\App\User::USER_ROLE, \App\User::ADMIN_ROLE]
        ]
    ],
    'edit_fields' => [
        'email' => [
            'type' => 'text'
        ],
        'country' => [
            'title' => 'Country',
            'type' => 'relationship'
        ],
        'role' => [
            'title' => 'Role',
            'type' => 'enum',
            'options' => [\App\User::USER_ROLE, \App\User::ADMIN_ROLE]
        ],
        /*'password' => [
            'type' => 'password',
            'title' => 'Пароль',
            'editable' => function($model)
            {
                return bcrypt($model->password);
            }
        ]*/
    ]
];