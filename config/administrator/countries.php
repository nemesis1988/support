<?php

return [
    'title' => 'Countries',
    'single' => 'country',
    'model' => \App\Country::class,
    'columns' => [
        'id',
        'name' => [
            'title' => 'Name',
            'type' => 'string'
        ],
    ],
    'edit_fields' => [
        'name' => [
            'title' => 'Name',
            'type' => 'text'
        ],
    ]
];