// Понимаю что гавнокод. Сделано так для экономии времени.
$(document).ready(function(){
    var inProgress = false;
    var page=2;
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height() && !inProgress) {
            $.ajax({
                url: '/question/new',
                dataType: 'json',
                method: 'GET',
                data: {"page" : page},
                beforeSend: function() {
                    inProgress = true;}
            }).done(function(data){
                if (data.data.length > 0) {
                    $.each(data.data, function(index, data){
                        $(".new-questions").append("<a href=\"/question/show/" + data.id + "\">" +
                            "<div class=\"list-group-item question\" id=\"" + data.id + "\">" +
                            "<h4 class=\"question-title\">" + data.title + "</h4>" +
                            "</div>" +
                            "</a>");
                    });
                    inProgress = false;
                    page += 1;
                }
            });
        }
    });
});