$('img.captcha-img').on('click', function () {
    var captcha = $(this);
    var config = captcha.data('refresh-config');
    $.ajax({
        method: 'GET',
        url: '/get_captcha/' + config,
    }).done(function (response) {
        captcha.prop('src', response);
    });
});
$('.question').on('click',function(){
    var id = this.id;
    $("#question_text_" + id).toggleClass('hidden');
});
